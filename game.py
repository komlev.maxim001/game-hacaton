import pygame
b_score = 0
f_score = 0
class Fruit(pygame.sprite.Sprite):
    def __init__(self, position_x: int, position_y: int):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("images/bon.png")
        self.rect = self.image.get_rect()
        self.rect.center = (self.image.get_width() / 2, self.image.get_height() / 2)
        self.position = pygame.Rect(position_x * 55, position_y * 55, self.rect.w, self.rect.h)

    def update(self, snake: list):
        global b_score
        self.rect.x = self.position.x
        self.rect.y = self.position.y
        if (self.rect.x // 55, self.rect.y // 55) == snake[0]:
            b_score += 1
            self.kill()

    def end(self):
        global b_score
        b_score = 0
        self.kill()


class Fish(pygame.sprite.Sprite):
    def __init__(self, position_x: int, position_y: int):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.image.load("images/fish.png")
        self.rect = self.image.get_rect()
        self.rect.center = (self.image.get_width() / 2, self.image.get_height() / 2)
        self.position = pygame.Rect(position_x * 55, position_y * 55, self.rect.w, self.rect.h)

    def update(self, snake: list):
        global f_score
        self.rect.x = self.position.x
        self.rect.y = self.position.y
        if (self.rect.x // 55, self.rect.y // 55) == snake[0]:
            f_score += 1
            self.kill()

    def end(self):
        global f_score
        f_score = 0
        self.kill()



