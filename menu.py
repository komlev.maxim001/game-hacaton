import pygame
from time import sleep
import game
class Menu():
    def __init__(self, screen_size: list):
        self.run_display = True
        self.display = screen_size[0] // 2, screen_size[1] // 2
        self.cursor_rect = pygame.Rect(200, 200, 200, 200)
        self.offset = 100
        self.screen = pygame.display.set_mode(screen_size)


    def draw_text(self, text, size, x, y):
        font = pygame.font.Font("game files/14722.ttf", size)
        text_surface = font.render(text, True, (0, 0, 0))
        text_rect = text_surface.get_rect()
        text_rect.center = (x, y)
        self.screen.blit(text_surface, text_rect)


    def draw_cursor(self, cursor_rect):
        self.draw_text("*", 40, cursor_rect[0], cursor_rect[1])

class Main_menu(Menu, pygame.sprite.Sprite):
    def __init__(self,  screen_size):
        Menu.__init__(self, screen_size)
        self.screen_size = screen_size
        self.image = pygame.image.load("images/bgg.png")
        self.image = pygame.transform.scale(self.image, screen_size)
        self.position = (screen_size[0] // 2, (0, screen_size[1] // 2))
        self.rect = self.image.get_rect()
        self.menu_point = 0

    def change_menu(self, point):
        if point == 0:
            return 0
        elif point == 1:
            return 1
        else:
            return 2


    def update_keys(self, keys):
        if keys.get(pygame.K_UP):
            if self.menu_point == 0:
                self.menu_point == 2
            else:
                self.menu_point -= 1
        if keys.get(pygame.K_DOWN):
            if self.menu_point == 2:
                self.menu_point == 0
            else:
                self.menu_point += 1


    def cursor_update(self, screen_size):
        if self.menu_point == 0:
            self.cursor_rect = [screen_size[0] // 2 - 190, screen_size[1] // 2 - 47]
        elif self.menu_point == 1:
            self.cursor_rect = [screen_size[0] // 2 - 328, screen_size[1] // 2 + 28]
        else:
            self.cursor_rect = [screen_size[0] // 2 - 227, screen_size[1] // 2 + 103]

    def update(self, keys):
        self.update_keys(keys)
        self.cursor_update(self.screen_size)
        Menu.draw_cursor(self, self.cursor_rect)


