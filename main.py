import pygame
import random
import game
from time import sleep
import menu
n = 0
rev = 0
f_pos = list()
cell_size = (55, 55)  # Размер 1 клетки
field_size = (24, 13)  # Размер поля

snake = [(10, 7), (10, 8), (10, 9)]
snake_velocity = (0, -1)

screen_size = [field_size[0] * cell_size[0], field_size[1] * cell_size[1] + 75]  # Размер экрана +Шкала сверху

def new_game():
    global dog_lifes, cat_lifes, score_f, score_b, time, snake, snake_velocity, frames, spawn_fish, spawn_fruit, dog
    dog_lifes = 3
    cat_lifes = 3
    score_f = 0
    dog = True
    spawn_fruit = False
    spawn_fish = False
    score_b = 0
    time = 0
    sprites[0] =pygame.image.load("images/dog_active.png")
    sprites[0] = pygame.transform.scale(sprites[0], cell_size)
    sprites[1] = pygame.image.load("images/cat_sleep.png")
    sprites[1] = pygame.transform.scale(sprites[1], cell_size)
    frames = 0
    snake = [(10, 7), (10, 8), (10, 9)]
    snake_velocity = (0, -1)


def blit_field(field):
    for y in range(field_size[1]):
        for x in range(field_size[0]):
            if field[y][x] is not None:
                if x % 3 == 0:
                    sprites[4] = pygame.transform.rotate(sprites[4], 90)
                elif y % 3 == 0:
                    sprites[4] = pygame.transform.rotate(sprites[4], 180)
                else:
                    sprites[4] = pygame.transform.rotate(sprites[4], -90)
            screen.blit(sprites[4], pygame.Rect(x * cell_size[0], y * cell_size[1], cell_size[0], cell_size[1]))


def gen_fruit(x):
    global f_pos, g_pos
    accept = True
    while accept:
        fruit_x = random.randint(0, field_size[0] - 1)
        fruit_y = random.randint(0, field_size[1] - 1)
        y = 0
        if x == 1:
            f_pos = [fruit_x, fruit_y]
        else:
            g_pos = [fruit_x, fruit_y]
        for k in range(len(snake)):
            if snake[k] != f_pos:
                y += 1
        if y == len(snake) :
            accept = not accept


def blit_fruit2():
    global g_pos, fish
    fish = game.Fish(g_pos[0], g_pos[1])
    fishes.add(fish)


def blit_fruit():
    global f_pos, bon
    bon = game.Fruit(f_pos[0], f_pos[1])
    bons.add(bon)


field_ground = []
for i in range(field_size[1]):
    row = []
    for j in range(field_size[0]):
        row.append(4)
    field_ground.append(row)

sprites = [
    pygame.image.load("images/dog_active.png"),
    pygame.image.load("images/cat_sleep.png"),
    pygame.image.load("images/body.png"),
    pygame.image.load("images/rotate.png"),
    pygame.image.load("images/dust2.jpg"),
    pygame.image.load("images/bon.png")]
def transform():
    for k in range(len(sprites)):
        sprites[k] = pygame.transform.scale(sprites[k], cell_size)


transform()
pygame.init()
pygame.mixer.init()
screen = pygame.display.set_mode(screen_size)
pygame.display.set_caption("CatDog: Desert adventure")
clock = pygame.time.Clock()
bons = pygame.sprite.Group()
fishes = pygame.sprite.Group()
menu = menu.Main_menu(screen_size)

is_run = True
is_main_menu = True
is_tutorial = False
is_pause = False
is_lose = True
is_game_menu = False
is_reverse = False
keys = {}


def draw_text(text, size, x, y):
    font = pygame.font.Font("game files/14722.ttf", size)
    text_surface = font.render(text, True, (105, 64, 27))
    text_rect = text_surface.get_rect()
    text_rect.center = (x, y)
    screen.blit(text_surface, text_rect)


def blit_tab(score, score2, time, d_life, c_life):
    tab = pygame.image.load("images/cov.png")
    screen.blit(tab, pygame.Rect(0, 790 - 75, 0, 0))
    draw_text("time: " + str(time), 35, 1190, 754)
    draw_text("x " + str(score), 27, 935, 754)
    draw_text("x " + str(score2), 27, 815, 754)
    draw_text("x " + str(d_life), 27, 145, 754)
    draw_text("x " + str(c_life), 27, 325, 754)


def reverse():
    global is_reverse, snake_velocity, snake, dog
    is_reverse = not is_reverse
    dog = not dog
    if is_reverse:
        sprites[0] = pygame.image.load("images/cat_active.png")
        sprites[1] = pygame.image.load("images/dog_sleep.png")
        transform()
    else:
        sprites[0] = pygame.image.load("images/dog_active.png")  # 0
        sprites[1] = pygame.image.load("images/cat_sleep.png")  # 1
        transform()
    if snake_velocity[1] == -1:
        snake_velocity = (0, 1)
    elif snake_velocity[1] == 1:
        snake_velocity = (0, -1)
    if snake_velocity[0] == -1:
        snake_velocity = (1, 0)
    elif snake_velocity[0] == 1:
        snake_velocity = (-1, 0)
    snake = snake[::-1]

def end():
    bon.end()
    fish.end()

while is_run:
    clock.tick(6)
    if is_main_menu:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_run = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    if menu.change_menu(menu.menu_point) == 0:
                        is_main_menu = not is_main_menu
                        new_game()
                        is_lose = not is_lose
                    elif menu.change_menu(menu.menu_point) == 1:
                        is_main_menu = not is_main_menu
                        is_tutorial = not is_tutorial
                    elif menu.change_menu(menu.menu_point) == 2:
                        is_run = False

                keys[event.key] = True
                sleep(0.2)

            if event.type == pygame.KEYUP:
                keys[event.key] = False
        #screen.fill((255, 255, 255))
        screen.blit(menu.image, menu.image.get_rect())
        menu.draw_text("Начать игру", 40, screen_size[0] // 2, screen_size[1] // 2 - 50)
        menu.draw_text("Правила и управление", 40, screen_size[0] // 2, screen_size[1] // 2 + 25)
        menu.draw_text("Выйти из игры", 40, screen_size[0] // 2, screen_size[1] // 2 + 100)
        menu.update(keys)


    elif is_tutorial:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    is_main_menu = not is_main_menu
                    is_tutorial = not is_tutorial
        pic = pygame.image.load("images/manual.png")
        pic = pygame.transform.scale(pic, screen_size)
        screen.blit(pic, pygame.Rect(0, 0, 0, 0))
    elif is_game_menu:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_run = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    is_game_menu = not is_game_menu
                keys[event.key] = True

            if event.type == pygame.KEYUP:
                keys[event.key] = False
        #screen.fill((20, 2, 20))
    elif is_pause:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_run = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    is_pause = not is_pause
        blit_tab(score_b, score_f, time, dog_lifes, cat_lifes)
        draw_text("Пауза", 35, 570, 754)
        pass
    elif is_lose:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_run = False
            if event.type == pygame.KEYUP:
                if event.key == pygame.K_ESCAPE:
                    is_main_menu = not is_main_menu
        pic = pygame.image.load("images/bg.png")
        pic = pygame.transform.scale(pic, screen_size)
        screen.blit(pic, pygame.Rect(0, 0, 0, 0))
        draw_text("Вы проиграли!", 50, screen_size[0] // 2, screen_size[1] // 2 - 100)
        draw_text("Ваш счет: " + str(score_f + score_b), 50, screen_size[0] // 2, screen_size[1] // 2)

    else:
        while spawn_fruit == False:
            gen_fruit(1)
            blit_fruit()
            spawn_fruit = not spawn_fruit
        while spawn_fish == False:
            gen_fruit(0)
            blit_fruit2()
            spawn_fish = not spawn_fish
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                is_run = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    is_pause = not is_pause
                keys[event.key] = True
                if event.key == pygame.K_LEFT:
                    if snake_velocity[0] != 1:
                        snake_velocity = (-1, 0)
                if event.key == pygame.K_RIGHT:
                    if snake_velocity[0] != -1:
                        snake_velocity = (1, 0)
                if event.key == pygame.K_UP:
                    if snake_velocity[1] != 1:
                        snake_velocity = (0, -1)
                if event.key == pygame.K_DOWN:
                    if snake_velocity[1] != -1:
                        snake_velocity = (0, 1)
                if event.key == pygame.K_TAB:
                    rev = 1
            if event.type == pygame.KEYUP:
                keys[event.key] = False

        #00screen.fill((0, 255, 255))
        blit_field(field_ground)
        if frames == 6:
            time += 1
            frames = 0
        frames += 1
        angle = 0
        if snake[0][0] < snake[1][0]:
            angle = 90
        elif snake[0][0] > snake[1][0]:
            angle = -90
        elif snake[0][1] > snake[1][1]:
            angle = 180


        head = pygame.transform.rotate(sprites[0], angle)
        screen.blit(head, pygame.Rect(snake[0][0] * cell_size[0], snake[0][1] * cell_size[1], cell_size[0], cell_size[1]))
        for i in range(1, len(snake) - 1, 1):
            sprite_id = 2
            angle = 0
            body = pygame.transform.rotate(sprites[2], angle)
            if snake[i][1] == snake[i - 1][1] and snake[i][1] == snake[i + 1][1]:
                angle = 90
            elif snake[i][0] == snake[i - 1][0] and snake[i][0] == snake[i + 1][0]:
                angle = 0
            else:
                sprite_id = 3
                if snake[i][1] == snake[i - 1][1] and snake[i][1] < snake[i + 1][1]:
                    sprite_id = 3
                    if snake[i][0] == snake[i + 1][0] and snake[i][0] < snake[i - 1][0]:
                        angle = 180
                    else:
                        angle = 90
                elif snake[i][1] == snake[i - 1][1] and snake[1][1] > snake[i + 1][1]:
                    sprite_id = 3
                    if snake[i][0] == snake[i + 1][0] and snake[i][0] < snake[i - 1][0]:
                        angle = -90
                elif snake[i][1] == snake[i + 1][1] and snake[i][1] < snake[i - 1][1]:
                    sprite_id = 3
                    if snake[i][0] == snake[i - 1][0] and snake[i][0] > snake[i + 1][0]:
                        angle = 90
                    else:
                        angle = 180
                elif snake[i][1] == snake[i + 1][1] and snake[i][1] > snake[i - 1][1]:
                    sprite_id = 3
                    if snake[i - 1][0] > snake[i + 1][0]:
                        angle = 0
                    else:
                        angle = -90
                elif snake[i][0] == snake[i + 1][0] and snake[i][0] < snake[i - 1][0] and snake[i][1] > snake[i +1][1]:
                    angle = -90
            body = pygame.transform.rotate(sprites[sprite_id], angle)
            screen.blit(body, pygame.Rect(snake[i][0] * cell_size[0], snake[i][1] * cell_size[1], cell_size[0], cell_size[1]))

        angle = 180
        if snake[-1][0] < snake[-2][0]:
            angle = -90
        elif snake[-1][0] > snake[-2][0]:
            angle = 90
        elif snake[-1][1] > snake[-2][1]:
            angle = 0
        tail = pygame.transform.rotate(sprites[1], angle)
        screen.blit(tail,
                    pygame.Rect(snake[-1][0] * cell_size[0], snake[-1][1] * cell_size[1], cell_size[0], cell_size[1]))
        pygame.display.flip()
        for i in range(len(snake) - 1, 0, -1):
            snake[i] = snake[i - 1]
        snake[0] = (snake[0][0] + snake_velocity[0], snake[0][1] + snake_velocity[1])
        if rev == 1:
            reverse()
            rev = 0
        blit_tab(score_b, score_f, time, dog_lifes, cat_lifes)
        if snake[0][0] * cell_size[0] < 0:
            is_lose = not is_lose
            dog = True
            end()
        elif snake[0][0] * cell_size[0] >= 1320:
            is_lose = not is_lose
            end()
            dog = True
        elif snake[0][1] * cell_size[0] < 0:
            is_lose = not is_lose
            end()
            dog = True
        elif snake[0][1] * cell_size[0] >= 790 - 75:
            is_lose = not is_lose
            end()
            dog = True
        elif dog_lifes == 0 or cat_lifes == 0:
            is_lose = not is_lose
            end()
            dog = True

        bons.update(snake)
        fishes.update(snake)
        if dog:
            if game.b_score > score_b:
                spawn_fruit = not spawn_fruit
                score_b += 1
                snake.append(snake[len(snake) - 1])
            if game.f_score > score_f:
                spawn_fish = not spawn_fish
                score_f += 1
                dog_lifes -= 1
                snake.append(snake[len(snake) - 1])
        else:
            if game.b_score > score_b:
                spawn_fruit = not spawn_fruit
                score_b += 1
                cat_lifes -= 1
                snake.append(snake[len(snake) - 1])
            if game.f_score > score_f:
                spawn_fish = not spawn_fish
                score_f += 1
                snake.append(snake[len(snake)-1])
        fishes.draw(screen)
        bons.draw(screen)

    pygame.display.update()
    pygame.display.flip()